package ru.t1.malyugin.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabasePropertyService {

    @NotNull
    String getDBUsername();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBFormatSQL();

    @NotNull
    String getDBShowSQL();

    @NotNull
    String getDBHmb2DDLAuto();

    @NotNull
    String getDBCacheUseSecondLevel();

    @NotNull
    String getDBCacheUseQueryCache();

    @NotNull
    String getDBCacheUseMinimalPuts();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBCacheProviderFile();

    @NotNull
    String getDBCacheRegionFactoryClass();

    @NotNull
    String getLiquibaseConfig();

    @NotNull
    String getInitToken();

}