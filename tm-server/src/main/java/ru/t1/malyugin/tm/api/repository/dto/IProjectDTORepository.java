package ru.t1.malyugin.tm.api.repository.dto;

import ru.t1.malyugin.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IWBSDTORepository<ProjectDTO> {

}
