package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.malyugin.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}