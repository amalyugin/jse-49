package ru.t1.malyugin.tm.api.repository.model;

import ru.t1.malyugin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}