package ru.t1.malyugin.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.ITokenService;

public class TokenService implements ITokenService {

    @Getter
    @Setter
    @Nullable
    private String token;

}